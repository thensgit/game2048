Клонируем репозиторий:

```
git clone https://gitlab.com/thensgit/game2048.git

cd game2048/

```

Для сборки докер образа выполняем команду:

```
sudo docker build . -t app2048

```
Существует баг, когда образ не собирается на виртуальной машине под vmware workstation, тогда к команде нужно добавить ключ --network=host

```
sudo docker build --network=host . -t app2048

```

Для запуска докер контейнера:

```
sudo docker run -p 127.0.0.1:80:80 -d --name app2048 app2048

```