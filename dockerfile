FROM node:16-alpine as builder
WORKDIR /app
COPY /app .
RUN npm install --iclude=dev && npm run build && npm cache clean --force

FROM nginx:1.25-alpine-slim
WORKDIR /usr/share/nginx/html
COPY --from=builder /app/dist .
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
